#include "window.h"
#include <stdio.h>

int CWindow::s_windowsCount = 0;
CWindow *CWindow::s_windows[CWindow::s_maxWindows];

LRESULT CALLBACK CWindow::s_handler(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
	CREATESTRUCT *cs;
	CWindow *w;

	// Handle the creation directly. This message is sent before 
	// CreateWindow has exited, so the class is not in the list
	// but can be retrieved from the creation struct
	if(msg == WM_NCCREATE)
	{
		cs = (CREATESTRUCT*)lp;
		w = (CWindow*)cs->lpCreateParams;
		return w->_handler(hWnd, msg, wp, lp);
	}

	// Find the associated window
	for(int i = 0; i < s_maxWindows; i++)
	{
		if(s_windows[i] != NULL && s_windows[i]->getHandle() == hWnd) 
			return s_windows[i]->_handler(hWnd, msg, wp, lp);
	}

	// Window not found
	return 0;
}

void CWindow::show()
{
	if(m_hWnd) 
	{
		ShowWindow(m_hWnd, SW_NORMAL);
		UpdateWindow(m_hWnd);
	}
}

void CWindow::destroy()
{
	if(m_hDC != NULL) ReleaseDC(m_hWnd, m_hDC);
	if(m_hWnd != NULL) DestroyWindow(m_hWnd);
	UnregisterClass(m_className, m_hInst);
	_reset();
}

void CWindow::_reset()
{
	m_width = 0;
	m_height = 0;
	m_hWnd = NULL;
	m_hDC = NULL;
	m_hInst = NULL;
	m_active = 0;
	m_created = 0;
}

int CWindow::create(HINSTANCE hInst, int width, int height, const char* windowName)
{
	WNDCLASS wc;
	RECT r;

	if(m_created)
		return 0;

	// Setup and register a new window class
	ZeroMemory(&wc, sizeof(wc));
	wc.hInstance = hInst;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.lpfnWndProc = (WNDPROC)s_handler;
	wc.lpszClassName = m_className;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	if(!RegisterClass(&wc))
	{
		// Class registration failed
		return 0;
	}
	
	// Setup window size
	r.left = 0;
	r.top = 0;
	r.right = width;
	r.bottom = height;

	// Adjust the size to have a client rect of WxH
	AdjustWindowRect(&r, WS_OVERLAPPEDWINDOW, false);

	// Create the window
	m_hWnd = CreateWindow(m_className, windowName, WS_OVERLAPPEDWINDOW, 
		CW_USEDEFAULT, CW_USEDEFAULT, r.right - r.left, r.bottom - r.top, NULL, NULL, hInst, this);

	// Fail
	if(m_hWnd == NULL) return 0;

	m_hInst = hInst;
	m_width = width;
	m_height = height;
	m_created = 1;

	return 1;
}

void CWindow::swapBuffers()
{
	SwapBuffers(getDC());
}

HWND CWindow::getHandle()
{
	return m_hWnd;
}

HDC CWindow::getDC()
{
	// Lazy context evaluation
	if(m_hDC == NULL && m_hWnd != NULL) 
		m_hDC = GetDC(m_hWnd);
	return m_hDC;
}

int CWindow::getActive()
{
	return m_active;
}

int CWindow::getWidth()
{
	return m_width;
}

int CWindow::getHeight()
{
	return m_height;
}

void CWindow::setTitle(const char *title)
{
	SetWindowText(m_hWnd, title);
}

LRESULT CALLBACK CWindow::_handler(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
	CREATESTRUCT *cs;

	// Forward the message
	switch(msg)
	{
	case WM_NCCREATE:
	case WM_CREATE:
		if(delegate) delegate->onWindowCreate();
		break;
	case WM_SIZE:
		m_width = LOWORD(lp);
		m_height = HIWORD(lp);
		if(delegate) delegate->onWindowResize(m_width, m_height);
		break;
	case WM_LBUTTONDOWN:
		if(delegate) delegate->onMouseDown(0, GET_X_LPARAM(lp), GET_Y_LPARAM(lp));
		break;
	case WM_LBUTTONUP:
		if(delegate) delegate->onMouseUp(0, GET_X_LPARAM(lp), GET_Y_LPARAM(lp));
		break;
	case WM_RBUTTONDOWN:
		if(delegate) delegate->onMouseDown(1, GET_X_LPARAM(lp), GET_Y_LPARAM(lp));
		break;
	case WM_RBUTTONUP:
		if(delegate) delegate->onMouseUp(1, GET_X_LPARAM(lp), GET_Y_LPARAM(lp));
		break;
	case WM_MOUSEMOVE:
		if(delegate) delegate->onMouseMove(GET_X_LPARAM(lp), GET_Y_LPARAM(lp));
		break;
	case WM_MOUSEWHEEL:
		if(delegate) delegate->onMouseWheel(GET_WHEEL_DELTA_WPARAM(wp));
		break;
	case WM_KEYDOWN:
		if(delegate) delegate->onKeyDown(wp);
		break;
	case WM_KEYUP:
		if(delegate) delegate->onKeyUp(wp);
		break;
	case WM_DESTROY:
		if(delegate) delegate->onWindowDestroy();
		break;
	case WM_ACTIVATE:
		// Save CPU and disable rendering when the window is inactive
		m_active = LOWORD(wp) != WA_INACTIVE;
		return 0;
	}

	return DefWindowProc(hWnd, msg, wp, lp);
}

CWindow::CWindow()
{
	delegate = NULL;
	_reset();

	// Lazy static inizialization
	if(s_windowsCount == 0)
	{
		memset(s_windows, 0, sizeof(s_windows));
	}

	s_windowsCount++;

	// Generate a unique class name
	sprintf_s(m_className, sizeof(m_className), "CWindow#%02d", s_windowsCount);
	
	// Add to the list (if a room is found)
	for(int i = 0; i < s_maxWindows; i++)
	{
		// TODO: handle the case when the list is full
		if(s_windows[i] == NULL)
		{
			s_windows[i] = this;
			break;
		}
	}
}

CWindow::~CWindow()
{
	// Just remove from the list (if found)
	for(int i = 0; i < s_maxWindows; i++)
	{
		if(s_windows[i] == this)
		{
			s_windows[i] = NULL;
			break;
		}
	}

	if(m_created) destroy();
}
