//uniform vec4 uColor;
varying vec3 vNorm;
varying vec3 vLightDir[8];
uniform sampler2D myTexture;
void main()
{
	vec4 diffuse;
	vec4 ambient;
	vec4 color = texture2D(myTexture, vec2(gl_TexCoord[0]));
	gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
	for(int i = 0; i < 8; i++)
	{
		diffuse = gl_LightSource[i].diffuse * clamp(dot(vNorm, vLightDir[i]), 0.0, 1.0);
		ambient = gl_LightSource[i].ambient;
		gl_FragColor.rgb += color * (ambient + diffuse);
	}
};
