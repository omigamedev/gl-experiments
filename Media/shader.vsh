varying vec3 vNorm;
varying vec3 vLightDir[8];
void main()
{
	vNorm = gl_NormalMatrix * gl_Normal;
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	vec3 mvVertex = gl_ModelViewMatrix * gl_Vertex;
	for(int i = 0; i < 8; i++)
	{
		vLightDir[i] = normalize(gl_LightSource[i].position - mvVertex);
	}
};
