#include "PngLoader.h"


CPngLoader::CPngLoader(void)
{
	twidth = theight = 0;
	bit_depth = 0;
	color_type = 0;
	image_data = NULL;
	name[0] = '\0';
}


CPngLoader::~CPngLoader(void)
{
	unload();
}

int CPngLoader::load(const char* fileName)
{
	FILE *fp;

	if((fp = fopen(fileName, "rb")) == NULL)
		return 0;

	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	png_infop info_ptr = png_create_info_struct(png_ptr);
	png_infop end_info = png_create_info_struct(png_ptr);

	// png error stuff, not sure libpng man suggests this.
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		return 0;
	}

	png_init_io(png_ptr, fp);
	png_read_info(png_ptr, info_ptr);
	png_get_IHDR(png_ptr, info_ptr, &twidth, &theight, &bit_depth, &color_type, NULL, NULL, NULL);
	
	GLint format;
	if(color_type == PNG_COLOR_TYPE_RGB) format = GL_RGB;
	if(color_type == PNG_COLOR_TYPE_RGBA) format = GL_RGBA;

	// Update the png info struct.
	png_read_update_info(png_ptr, info_ptr);

	// Row size in bytes.
	int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

	// Allocate the image_data as a big block, to be given to opengl
	image_data = new png_byte[rowbytes * (theight)];
	if (!image_data)
	{
		// clean up memory and close stuff
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		return 0;
	}

	//row_pointers is for pointing to image_data for reading the png with libpng
	png_bytep *row_pointers = new png_bytep[sizeof(png_bytep) * theight];
	if (!row_pointers)
	{
		//clean up memory and close stuff
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		free(image_data);
		return 0;
	}
	// set the individual row_pointers to point at the correct offsets of image_data
	for(int i = 0; i < theight; ++i)
		//row_pointers[i] = image_data + i * rowbytes;
		row_pointers[theight - i - 1] = image_data + i * rowbytes; // invert rows
	
	//read the png into image_data through row_pointers
	png_read_image(png_ptr, row_pointers);

	//clean up memory and close stuff
	png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
	delete[] row_pointers;

	return 1;
}

int CPngLoader::hasAlpha()
{
	return color_type & PNG_COLOR_MASK_ALPHA;
}

int CPngLoader::hasColor()
{
	return color_type & PNG_COLOR_MASK_COLOR;
}

unsigned char* CPngLoader::getData()
{
	return image_data;
}

int CPngLoader::getWidth()
{
	return twidth;
}

int CPngLoader::getHeight()
{
	return theight;
}

void CPngLoader::unload()
{
	if(image_data) delete[] image_data;
}