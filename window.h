#pragma once

#include <Windows.h>
#include <WindowsX.h>

class CWindowDelegate
{
	friend class CWindow;

	virtual void onWindowCreate() {}
	virtual void onWindowDestroy() {}
	virtual void onWindowResize(int width, int height) {}
	virtual void onMouseDown(int button, int x, int y) {}
	virtual void onMouseMove(int x, int y) {}
	virtual void onMouseUp(int button, int x, int y) {}
	virtual void onMouseWheel(int delta) {}
	virtual void onKeyDown(int key) {}
	virtual void onKeyUp(int key) {}
};

class CWindow
{
public:
	static const int s_maxWindows = 10;

	// Constructors
	CWindow();

	// Virtual destructor
	virtual ~CWindow();

	int create(HINSTANCE hInst, int width, int height, const char* windowName);
	void show();
	void destroy();
	void swapBuffers();

	// Public members
	CWindowDelegate *delegate;

	// Getters
	HDC getDC();
	HWND getHandle();
	int getWidth();
	int getHeight();
	int getActive();
	const char *getClassName();

	// Setters
	void setTitle(const char* title);

protected:
	virtual LRESULT CALLBACK _handler(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp);

private:
	static LRESULT CALLBACK s_handler(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp);
	static int s_windowsCount;
	static CWindow *s_windows[s_maxWindows];

	HINSTANCE m_hInst;
	HWND m_hWnd;
	HDC m_hDC;
	int m_width;
	int m_height;
	int m_active;
	char m_className[64];
	int m_created;

	void _reset();
};
