#include "ObjModel.h"
#include <vector>

CObjModel::CObjModel(void)
{
	m_vertex = NULL;
	m_loaded = 0;
	m_nvertex = 0;
}

CObjModel::~CObjModel(void)
{
	unload();
}

void CObjModel::unload()
{
	if(m_vertex) delete[] m_vertex;
	m_loaded = 0;
}

int CObjModel::load(const char* fileName)
{
	FILE *fp;
	char line[64];
	std::vector<vec3> vv, vn;
	std::vector<vec2> vt;
	std::vector<Vertex> out;
	float x, y, z;
	unsigned int fv1, fv2, fv3, ft1, ft2, ft3, fn1, fn2, fn3;
	unsigned int type;

	if((fp = fopen(fileName, "rb")) == NULL)
		return 0;

	// Delete previous data (if any)
	if(m_loaded) unload();

	while(fgets(line, sizeof(line), fp) != NULL)
	{
		type = line[0] + line[1];
		switch(type)
		{
		case 'v'+' ':
			sscanf(line, "v %f %f %f", &x, &y, &z);
			vv.push_back(vec3(x, y, z));
			break;
		case 'v'+'n':
			sscanf(line, "vn %f %f %f", &x, &y, &z);
			vn.push_back(vec3(x, y, z));
			break;
		case 'v'+'t':
			sscanf(line, "vt %f %f", &x, &y);
			vt.push_back(vec2(x, y));
			break;
		case 'f'+' ':
			sscanf(line, "f %d/%d/%d %d/%d/%d %d/%d/%d", &fv1, &ft1, &fn1, &fv2, &ft2, &fn2, &fv3, &ft3, &fn3);
			out.push_back(Vertex(vv[fv1-1], vn[fn1-1], vt[ft1-1]));
			out.push_back(Vertex(vv[fv2-1], vn[fn2-1], vt[ft2-1]));
			out.push_back(Vertex(vv[fv3-1], vn[fn3-1], vt[ft3-1]));
			break;
		}
	}

	fclose(fp);

	m_nvertex = out.size();
	m_vertex = new Vertex[out.size()];
	std::copy(out.begin(), out.end(), m_vertex);

	return 1;
}

const Vertex* CObjModel::getVertexPointer()
{
	return m_vertex;
}

int CObjModel::getVertexCount()
{
	return m_nvertex;
}
