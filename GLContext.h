#pragma once
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLu32.lib")
#pragma comment(lib, "glew32.lib")

#include <Windows.h>
#include <glew.h>
#include <gl\GL.h>
#include <gl\GLU.h>

class CGLContext
{
public:
	CGLContext();
	virtual ~CGLContext();

	int create(HDC hDC);
	void destroy();
	void init(int isOrtho, int vpWidth, int vpHeight);
	void reshape(int vpWidth, int vpHeight);

private:
	HGLRC m_hRC;
	HDC m_hDC;
	int m_ortho;

	void _reset();
};
