#pragma once
#pragma comment(lib, "zdll.lib")

#include <png.h>
#include <Windows.h>
#include <gl\GL.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

class CPngLoader
{
public:
	CPngLoader(void);
	~CPngLoader(void);

	int load(const char* filaName);
	void unload();
	int hasAlpha();
	int hasColor();
	int getWidth();
	int getHeight();
	unsigned char* getData();

private:
	png_uint_32 twidth, theight;
	int bit_depth, color_type;
	png_byte *image_data;
	char name[256];
};
