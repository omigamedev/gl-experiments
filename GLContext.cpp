#include "GLContext.h"

CGLContext::CGLContext()
{
}

CGLContext::~CGLContext()
{
}

void CGLContext::_reset()
{
	m_hRC = NULL;
	m_hDC = NULL;
}

int CGLContext::create(HDC hDC)
{
	// Setup and select pixel format
	static PIXELFORMATDESCRIPTOR pfd =	// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),	// Size Of This Pixel Format Descriptor
		1,								// Version Number
		PFD_DRAW_TO_WINDOW |			// Format Must Support Window
		PFD_SUPPORT_OPENGL |			// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,				// Must Support Double Buffering
		PFD_TYPE_RGBA,					// Request An RGBA Format
		24,								// Select Our Color Depth
		0, 0, 0, 0, 0, 0,				// Color Bits Ignored
		0,								// No Alpha Buffer
		0,								// Shift Bit Ignored
		0,								// No Accumulation Buffer
		0, 0, 0, 0,						// Accumulation Bits Ignored
		16,								// 16Bit Z-Buffer (Depth Buffer)
		0,								// No Stencil Buffer
		0,								// No Auxiliary Buffer
		PFD_MAIN_PLANE,					// Main Drawing Layer
		0,								// Reserved
		0, 0, 0							// Layer Masks Ignored
	};

	// Select the pixel format
	int pixel_format = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, pixel_format, &pfd);

	// Create the GL context
	m_hRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, m_hRC);

	m_hDC = hDC;

	return 1;
}

void CGLContext::destroy()
{
	// Destroy everything
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(m_hRC);
}

void CGLContext::init(int isOrtho, int vpWidth, int vpHeight)
{
	m_ortho = isOrtho;
	reshape(vpWidth, vpHeight);
}

void CGLContext::reshape(int vpWidth, int vpHeight)
{
	// Setup viewport and projection matrix
	glViewport(0, 0, vpWidth, vpHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(m_ortho) glOrtho(-1.0, 1.0, -1.0, 1.0, -10.0, 1000.0);
	else gluPerspective(45.0, (GLdouble)vpWidth / (GLdouble)vpHeight, 1.0, 1000.0);

	// Back to the modelview matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
