#pragma once

#include <stdio.h>
#include "3dmath.h"

class Vertex
{
public:
	vec3 pos;
	vec3 norm;
	vec2 tex;
	Vertex() {};
	Vertex(const vec3 &pos, const vec3 &norm, const vec2 &tex) 
		{ this->pos = pos; this->norm = norm; this->tex = tex; }
};

class CObjModel
{
public:
	CObjModel(void);
	~CObjModel(void);

	int load(const char* fileName);
	void unload();

	const Vertex* getVertexPointer();
	int getVertexCount();

private:
	Vertex *m_vertex;
	int m_loaded;
	int m_nvertex;
};
