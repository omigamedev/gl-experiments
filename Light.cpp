#include "Light.h"

CLight::CLight(void)
{
	m_enabled = 0;
}


CLight::~CLight(void)
{
}

void CLight::enable()
{
	if(!m_enabled)
	{
		glEnable(m_id);
		m_enabled = 1;
	}
}
void CLight::disable()
{
	if(m_enabled)
	{
		glDisable(m_id);
		m_enabled = 0;
	}
}

void CLight::toggle()
{
	m_enabled ? disable() : enable();
}

void CLight::update()
{
	static float pos[4];
	glLightfv(m_id, GL_AMBIENT, (GLfloat*)&m_ambient);
	glLightfv(m_id, GL_DIFFUSE, (GLfloat*)&m_diffuse);

	pos[0] = m_pos.x;
	pos[1] = m_pos.y;
	pos[2] = m_pos.z;
	pos[3] = 1.0f;
	glLightfv(m_id, GL_POSITION, pos);
}

void CLight::draw()
{
	const float scale = 1.0f;

	glPushMatrix();
	
	glTranslatef(m_pos.x, m_pos.y, m_pos.z);
	glScalef(scale, scale, scale);

	glBegin(GL_QUADS);
	// Front Face
	glVertex3f(-1.0f, -1.0f,  1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);
	// Back Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f, -1.0f);
	// Top Face
	glVertex3f(-1.0f,  1.0f, -1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);
	// Bottom Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);
	glVertex3f(-1.0f, -1.0f,  1.0f);
	// Right face
	glVertex3f( 1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);
	// Left Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f, -1.0f);
	glEnd();

	glPopMatrix();
}
