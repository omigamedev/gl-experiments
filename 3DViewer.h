#pragma once

#include "window.h"
#include "GLContext.h"
#include "ObjModel.h"
#include "3dmath.h"
#include "PngLoader.h"
#include "Light.h"
#include <sys/stat.h>
#include <sys/types.h>

typedef enum C3DViewerState
{
	IDLE,
	DRAG
} C3DViewerState;

class C3DViewer : public CWindowDelegate
{
public:
	C3DViewer();

	void start(HINSTANCE hInstance);
	void stop();
	void update();
	int loadTexture(const char* fileName);
	int loadShader(const char* fileName, GLenum type);
	int createProgram(GLuint vs, GLuint fs);
	int reloadShader();
	time_t isNewer(const char* fileName, time_t oldTime);

private:
	//void onWindowCreate();
	void onWindowDestroy();
	void onWindowResize(int width, int height);
	void onMouseDown(int button, int x, int y);
	void onMouseMove(int x, int y);
	void onMouseUp(int button, int x, int y);
	void onMouseWheel(int delta);
	void onKeyDown(int key);
	void onKeyUp(int key);
	
	CWindow wnd;
	CGLContext gl;
	CObjModel obj;
	CLight l1, l2;

	C3DViewerState m_state;
	int m_drag;
	point m_dragStart;
	vec3 m_dragRotStart;
	vec3 m_rot;
	vec4 m_fogColor;
	float m_zoom;
	float m_lightTheta;
	time_t m_vsLastTime;
	time_t m_fsLastTime;

	GLuint m_texID;
};
