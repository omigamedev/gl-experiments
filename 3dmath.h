#pragma once

#include <math.h>
#include <xmmintrin.h>

class point
{
public:
	int x, y;
	point() : x(0), y(0) {}
	void set(int x, int y) { this->x = x; this->y = y; }
};

class vec4
{
public:
	float x, y, z, w;
	
	vec4() { x=y=z=w = 0.0f; }
	vec4(float xx, float yy, float zz, float ww) { x = xx; y = yy; z = zz; w = ww; }

	void set(float xx, float yy, float zz, float ww) { x = xx; y = yy; z = zz; w = ww; }
};

class vec2
{
public:
	float x, y;

	vec2() : x(0.0f), y(0.0f) {}
	vec2(float x, float y) : x(x), y(y) {}

	void set(float x, float y) { this->x = x; this->y = y; }
	
	vec2 operator+(const vec2 &v) { return vec2(x + v.x, y + v.y); }
	vec2 operator-(const vec2 &v) { return vec2(x - v.x, y - v.y); }
};

class vec3
{
public:
	float x, y, z;

	vec3() : x(0.0f), y(0.0f), z(0.0f) {}
	vec3(float x, float y, float z) : x(x), y(y), z(z) {}

	void set(float x, float y, float z) { this->x = x; this->y = y; this->z = z; }
	
	vec3 operator+(const vec3 &v) { return vec3(x + v.x, y + v.y, z + v.z); }
	vec3 operator-(const vec3 &v) { return vec3(x - v.x, y - v.y, z - v.z); }
	vec3 operator-() { return vec3(-x, -y, -z); }
	vec3 operator^(const vec3 &v) const
	{ return vec3(y * v.z - z * v.y, z * v.x - x * v.z,	x * v.y - y * v.x); }
	float operator*(const vec3 &v) { return x * v.x + y * v.y + z * v.z; }
	int operator==(const vec3 &v) { return x == v.x && y == v.y && z == v.z; }
};
