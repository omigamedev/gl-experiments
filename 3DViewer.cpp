#include "3DViewer.h"

#define VSHADER "Media/shader.vsh"
#define FSHADER "Media/shader.fsh"

GLuint vsID = 0;
GLuint fsID = 0;
GLuint progID = 0;

C3DViewer::C3DViewer()
{
	m_drag = 0;
	m_zoom = -10.0f;
	m_lightTheta = 0.0f;
}

void C3DViewer::start(HINSTANCE hInstance)
{
	wnd.delegate = this;
	wnd.create(hInstance, 800, 600, "OpenGL - 3DViewer");
	gl.create(wnd.getDC());
	glewInit();
	wnd.show();

	// Load assets
	obj.load("Media/face.obj");
	m_texID = loadTexture("Media/face.png");
	
	gl.init(0, wnd.getWidth(), wnd.getHeight());
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_DEPTH_TEST);
	//glCullFace(GL_FRONT);
	glEnable(GL_CULL_FACE);
	glShadeModel(GL_SMOOTH); 
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Blending
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	// Fog
	//m_fogColor.set(0.0f, 0.0f, 0.0f, 1.0f);
	//glFogi(GL_FOG_MODE, GL_LINEAR);
	//glFogfv(GL_FOG_COLOR, (GLfloat*)&m_fogColor);
	//glFogf(GL_FOG_DENSITY, 0.035f);
	//glHint(GL_FOG_HINT, GL_DONT_CARE);
	//glFogf(GL_FOG_START, 10.0f);
	//glFogf(GL_FOG_END, 100.0f);
	//glEnable(GL_FOG);

	// Init the light
	glEnable(GL_LIGHTING);
	l1.m_id = GL_LIGHT0;
	l1.enable();
	l1.m_diffuse.set(0.5f, 0.5f, 0.5f, 1.0f);
	l1.m_ambient.set(0.0f, 0.0f, 0.0f, 1.0f);
	l2.m_id = GL_LIGHT1;
	l2.enable();
	l2.m_diffuse.set(0.5f, 0.5f, 0.5f, 1.0f);
	l2.m_ambient.set(0.3f, 0.3f, 0.3f, 1.0f);

	// Init texture
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_texID);

	// Init shader
	if(!reloadShader()) stop();
}

void C3DViewer::stop()
{
	// When the window is destroyed post a WM_QUIT message
	PostQuitMessage(0);
	gl.destroy();
	wnd.destroy();
}

time_t C3DViewer::isNewer(const char* fileName, time_t oldTime)
{
	struct stat fileStat;
	stat(fileName, &fileStat);
	return fileStat.st_mtime > oldTime;
}

void C3DViewer::update()
{
	static int frames = 0;
	if(frames == 50)
	{
		frames = 0;
		if(isNewer(VSHADER, m_vsLastTime) || isNewer(FSHADER, m_fsLastTime))
			reloadShader();
	}
	frames++;

	//if(!wnd.getActive()) return;

	// Draw GL scene
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//angle += 1.0f;
	m_lightTheta += 0.01f;

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, m_zoom);
	glRotatef(m_rot.x, 1.0f, 0.0f, 0.0f);
	glRotatef(m_rot.y, 0.0f, 1.0f, 0.0f);
	glRotatef(m_rot.z, 0.0f, 0.0f, 1.0f);
	//glScalef(0.3f, 0.3f, 0.3f);
	
	l1.m_pos.set(cosf(m_lightTheta) * 30.0f, 0.0f, sinf(m_lightTheta) * 30.0f);
	l1.update();
	l1.draw();

	//l2.m_pos.set(0.0f, cosf(m_lightTheta * 3.0f) * 30.0f, sinf(m_lightTheta * 3.0f) * 30.0f);
	l2.m_pos.set(cosf(-m_lightTheta * 3.0f) * 30.0f, 0.0f, sinf(-m_lightTheta * 3.0f) * 30.0f);
	l2.update();
	l2.draw();

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &obj.getVertexPointer()->pos);
	glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &obj.getVertexPointer()->tex);
	glNormalPointer(GL_FLOAT, sizeof(Vertex), &obj.getVertexPointer()->norm);

	glUseProgram(progID);

	GLint curProgID;

	// Disable if the program is in use
	glGetIntegerv(GL_CURRENT_PROGRAM, &curProgID);

	//glUniform4f(glGetUniformLocation(progID, "uColor"), 1.0f, 0.0f, 0.0f, 0.10f);
	glBindTexture(GL_TEXTURE_2D, m_texID);
	glDrawArrays(GL_TRIANGLES, 0, obj.getVertexCount());
	glUseProgram(0);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	wnd.swapBuffers();
}

int C3DViewer::loadTexture(const char* fileName)
{
	GLuint tid;
	CPngLoader png;

	if(!png.load(fileName)) return 0;

	GLuint format = png.hasAlpha() ? GL_RGBA : GL_RGB;
	glGenTextures(1, &tid);
	glBindTexture(GL_TEXTURE_2D, tid);
	glTexImage2D(GL_TEXTURE_2D, 0, format, png.getWidth(), png.getHeight(), 0, 
		format, GL_UNSIGNED_BYTE, (GLvoid*)png.getData());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	return 1;
}

int C3DViewer::loadShader(const char* fileName, GLenum type)
{
	FILE *fp;
	char* buffer;
	int len;
	int read;
	GLint res;
	GLuint id;
	
	// Open the file
	if(!(fp = fopen(fileName, "r")))
		return 0;

	// Read the content into a buffer
	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	rewind(fp);
	buffer = new char[len];
	ZeroMemory(buffer, len);
	read = fread(buffer, 1, len, fp);
	fclose(fp);

	if(read == 0)
	{
		delete[] buffer;
		return 0;
	}

	// Create the shader
	id = glCreateShader(type);
	glShaderSource(id, 1, (const GLchar**)&buffer, NULL);
	glCompileShader(id);
	glGetShaderiv(id, GL_COMPILE_STATUS, &res);
	if(res == GL_FALSE) 
	{
		delete[] buffer;
		return 0;
	}

	// Delete the buffer
	delete[] buffer;

	return id;
}

int C3DViewer::createProgram(GLuint vs, GLuint fs)
{
	GLint res;
	GLuint id;
	
	id = glCreateProgram();
	glAttachShader(id, vs);
	glAttachShader(id, fs);
	glLinkProgram(id);
	glGetShaderiv(id, GL_LINK_STATUS, &res);
	
	if(res == GL_FALSE) 
		return 0;
	
	return id;
}

int C3DViewer::reloadShader()
{
	//GLint curProgID;

	// Disable if the program is in use
	//glGetIntegerv(GL_CURRENT_PROGRAM, &curProgID);
	//if(curProgID == progID)
	
	// Disable the program and delete
	glUseProgram(0);
	if(vsID) glDeleteShader(vsID);
	if(fsID) glDeleteShader(fsID);
	if(progID) glDeleteProgram(progID);

	// Reload shaders and program
	vsID = loadShader(VSHADER, GL_VERTEX_SHADER);
	fsID = loadShader(FSHADER, GL_FRAGMENT_SHADER);
	if(!(vsID && fsID))
		return 0;
	
	progID = createProgram(vsID, fsID);
	if(!progID) 
		return 0;
	
	// If ok use the program just created
	glUseProgram(progID);

	struct stat fileStat;
	stat(VSHADER, &fileStat);
	m_vsLastTime = fileStat.st_mtime;
	stat(FSHADER, &fileStat);
	m_fsLastTime = fileStat.st_mtime;

	return 1;
}

void C3DViewer::onWindowDestroy()
{
	stop();
}

void C3DViewer::onWindowResize(int width, int height)
{
	gl.reshape(width, height);
}

void C3DViewer::onMouseMove(int x, int y)
{
	//char str[128];
	//sprintf(str, "mouse at %4dx%4d %s", x, y, m_drag ? "dragging" : "");
	//wnd.setTitle(str);

	if(m_drag)
	{
		float dx = (float)(x - m_dragStart.x) * 1.0f;
		float dy = (float)(y - m_dragStart.y) * 1.0f;
		m_rot.set(m_dragRotStart.x + dy, m_dragRotStart.y + dx, 0.0f);
	}
}

void C3DViewer::onMouseDown(int button, int x, int y)
{
	m_drag = 1;
	m_dragStart.set(x, y);
	m_dragRotStart = m_rot;
	SetCapture(wnd.getHandle());
}

void C3DViewer::onMouseUp(int button, int x, int y)
{
	m_drag = 0;
	ReleaseCapture();
}

void C3DViewer::onMouseWheel(int delta)
{
	m_zoom += (float)delta * 0.03f;
}

void C3DViewer::onKeyDown(int key)
{
	static int lightEnabled = 1;

	switch(key)
	{
	case VK_ESCAPE:
		stop();
		break;
	case 'L':
		if(lightEnabled) glDisable(GL_LIGHTING);
		else glEnable(GL_LIGHTING);
		lightEnabled = !lightEnabled;
		break;
	}
}

void C3DViewer::onKeyUp(int key)
{
	switch(key)
	{
	case VK_SPACE:
		reloadShader();
		break;
	}
}
