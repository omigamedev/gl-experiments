#include "3DViewer.h"

BOOL running = true;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, char*, int)
{
	MSG msg;
	C3DViewer app;

	app.start(hInstance);

	while(running)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// If WM_QUIT message was posted clean up and exit
			if(msg.message == WM_QUIT)
			{
				running = false;
			}
			else
			{
				// Process windows messages
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else 
		{
			app.update();
		}
	}

	return 0;
}

