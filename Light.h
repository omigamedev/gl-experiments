#pragma once

#include <Windows.h>
#include <gl\GL.h>
#include "3dmath.h"

class CLight
{
public:
	CLight(void);
	~CLight(void);

	void enable();
	void disable();
	void toggle();
	void update();
	void draw();

	vec4 m_ambient;
	vec4 m_diffuse;
	vec3 m_pos;
	GLenum m_id;
	int m_enabled;
};
